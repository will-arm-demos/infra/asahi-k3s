# Asahi K3s

Super simple ALARM package for installing and updating k3s on Asahi Linux. This installation is based on how BuzzCrate deploys in [ConfigBuzz](https://gitlab.com/buzzcrate/configbuzz). 

## Installation

1. Checkout this repo
1. `cd` into the folder
1. `makepkg -si`
1. Type your sudo password when prompted
1. To start and enable on boot, `systectl enable k3s --now`

### Set Local user

To set the local user to have access via Kubectl. 

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/rancher/k3s/k3s.yaml $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Wanna Contibute?

I get lazy, so pull requests are always open. Want to update k3s? Bump the `pkgver` value in the `PKGBUILD`, then run `makepkg -g >> PKGBUILD`. Keep the bottom sha256sums and delete the old ones above. 

The new `PKGBUILD` can either be submitted as a PR or like... update and use it yourself. 

Enjoy! 
